const jwt = require("jsonwebtoken");
const fs = require("fs");
const PRIVATE_KEY = fs.readFileSync(
  __dirname + "/../../config/jwt/private.pem",
  "utf8"
);
const PUBLIC_KEY = fs.readFileSync(
  __dirname + "/../../config/jwt/public.pem",
  "utf8"
);

function getTokenData(context) {
  const Authorization = context.request.get("Authorization");
  try {
    if (Authorization) {
      const token = Authorization.replace("Bearer ", "");
      const data = jwt.verify(token, PUBLIC_KEY, { algorithms: ["RS256"] });
      return data;
    }
  } catch (e) {
    throw new Error("Not authenticated");
  }
  

  throw new Error("Not authenticated");
}

module.exports = {
  getTokenData,
  PRIVATE_KEY,
  PUBLIC_KEY,
};
