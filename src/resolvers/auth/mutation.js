const argon2 = require("argon2");
const jwt = require("jsonwebtoken");
const { v4: uuidv4 } = require("uuid");
const { PRIVATE_KEY } = require("../../utils/jwt");

async function signup(parent, args, context, info) {
  const password = await argon2.hash(args.password);
  const roles = ["ROLE_USER"];
  switch(args.role) {
    case "investor":
      roles.push("ROLE_INVESTOR");
      break;
    case "entrepreneur":
      roles.push("ROLE_ENTREPRENEUR");
      break;
    case "admin":
      roles.push("ROLE_ADMIN");
      break;
  }
  delete args.role;
  const date = new Date();
  const user = await context.prisma.user.create({
    data: {
      ...args,
      password,
      roles,
      id: uuidv4(),
      createdAt: date,
      updatedAt: date,
      status: 0,
    },
  });

  const token = jwt.sign(
    { roles: user.role, username: user.email },
    {
      key: PRIVATE_KEY,
      passphrase: process.env.JWT_PASSPHRASE,
    },
    {
      algorithm: "RS256",
      expiresIn: "1h",
    }
  );
  return {
    token,
    user,
  };
}

async function login(parent, args, context, info) {
  const user = await context.prisma.user.findUnique({
    where: { email: args.email },
  });
  if (!user) {
    throw new Error("No such user found");
  }
  const valid = await argon2.verify(user.password, args.password);
  if (!valid) {
    throw new Error("Invalid password");
  }

  const token = jwt.sign(
    { roles: user.roles, username: user.email },
    {
      key: PRIVATE_KEY,
      passphrase: process.env.JWT_PASSPHRASE,
    },
    {
      algorithm: "RS256",
      expiresIn: "1h",
    }
  );

  return {
    token,
    user,
  };
}

module.exports = {
  signup,
  login,
};
