const Mutation = require("./mutation");

const resolvers = {
  Mutation,
};

module.exports = resolvers;
