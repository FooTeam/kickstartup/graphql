const { arrayCamelToSnakeCase } = require("../../helpers/array");
const { getTokenData } = require("../../utils/jwt");

async function updateUser(parent, args, context, info) {
  const author = await context.prisma.user.findUnique({
    where: { email: getTokenData(context).username },
  });
  const user = await context.prisma.user.findUnique({
    where: { id: args.user.id },
  });
  if (user.id === author.id || author.roles.contains("ROLE_ADMIN")) {
    return await context.prisma.user.update({
      where: { id: args.user.id },
      data: arrayCamelToSnakeCase(args.user),
    });
  }
  return user;
}

module.exports = {
  updateUser,
};
