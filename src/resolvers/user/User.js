async function company(parent, args, context) {
  return context.prisma.user.findUnique({ where: { id: parent.id } }).company();
}

async function application(parent, args, context) {
  return context.prisma.user
    .findUnique({ where: { id: parent.id } })
    .application();
}

async function review(parent, args, context) {
  return context.prisma.application_user.findMany({
    where: { user_id: parent.id },
  });
}

async function responseNotification(parent) {
  return parent.response_notification;
}

async function applyNotification(parent) {
  return parent.apply_notification;
}

async function donationNotification(parent) {
  return parent.donation_notification;
}
module.exports = {
  company,
  application,
  review,
  responseNotification,
  applyNotification,
  donationNotification,
};
