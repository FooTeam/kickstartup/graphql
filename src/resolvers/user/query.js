const { getTokenData } = require("../../utils/jwt");

function me(parent, args, context, info) {
  let data = getTokenData(context);
  return context.prisma.user.findUnique({
    where: { email: data.username },
  });
}

async function user(parent, args, context, info) {
  getTokenData(context);
  return await context.prisma.user.findUnique({
    where: { id: args.id },
  });
}

function getAllUsers(parent, args, context) {
    getTokenData(context);

    return context.prisma.user.findMany();
}

module.exports = {
    me,
    getAllUsers,
    user,
};
