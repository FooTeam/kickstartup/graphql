const User = require("./user/User");
const Application = require("./application/Application");
const Discussion = require("./discussion/Discussion");
const Message = require("./message/Message");
const Project = require("./project/Project");

const authResolvers = require("./auth");
const userResolvers = require("./user");
const projectResolvers = require("./project");
const mediaResolvers = require("./media");
const discussionResolvers = require("./discussion");
const messageResolvers = require("./message");
const companyResolvers = require("./company");
const applicationResolvers = require("./application");

const Mutation = {
  ...authResolvers.Mutation,
  ...projectResolvers.Mutation,
  ...mediaResolvers.Mutation,
  ...discussionResolvers.Mutation,
  ...messageResolvers.Mutation,
  ...userResolvers.Mutation,
  ...companyResolvers.Mutation,
  ...applicationResolvers.Mutation,
};

const Query = {
  ...projectResolvers.Query,
  ...userResolvers.Query,
  ...discussionResolvers.Query,
  ...messageResolvers.Query,
};

const resolvers = {
  Query,
  Mutation,
  User,
  Application,
  Project,
  Discussion,
  Message,
};

module.exports = {
  resolvers,
};
