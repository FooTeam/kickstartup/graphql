const Mutation = require("./mutation");
const Query = require("./query");

const resolvers = {
  Mutation,
  Query,
};

module.exports = resolvers;
