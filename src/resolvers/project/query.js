function project(parent, args, context) {
  return context.prisma.project.findUnique({
    where: { id: args.id },
  });
}

function getAllProjects(parent, args, context) {
  return context.prisma.project.findMany();
}

module.exports = {
  project,
  getAllProjects,
};
