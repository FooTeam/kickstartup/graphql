async function company(parent, args, context) {
    return context.prisma.project.findUnique({ where: { id: parent.id } }).company();
}

async function phoneNumber(parent) {
    return parent.phone_number;
}

module.exports = {
    company,
    phoneNumber,
}