const { getTokenData } = require("../../utils/jwt");
const { v4: uuidv4 } = require("uuid");
const { arrayCamelToSnakeCase } = require("../../helpers/array")

async function createProject(parent, args, context, info) {
  const company = await context.prisma.user.findUnique({
    where: { email: getTokenData(context).username },
  }).company();

  return await context.prisma.project.create({
      data: {
        ...arrayCamelToSnakeCase(args.project),
        company: {
          connect: { id: company[0].id }
        },
        id: uuidv4(),
        status: 0,
      },
    });
}

async function deleteProject(parent, args, context, info) {
  const user = await context.prisma.user.findUnique({
    where: { email: getTokenData(context).username },
  });
  const project = await context.prisma.project.findUnique({
    where: { id: args.id },
  });
  if (user.id === project.owner_id) {
    await context.prisma.project.delete({
      where: { id: args.id },
    });
    return true;
  }
  return false;
}

async function updateProject(parent, args, context, info) {
  const user = await context.prisma.user.findUnique({
    where: { email: getTokenData(context).username },
  });
  return {
    ...(await context.prisma.project.update({
      where: { id: args.project.id },
      data: { ...arrayCamelToSnakeCase(args.project) },
    })),
    User: user,
  };
  return false;
}

module.exports = {
  createProject,
  deleteProject,
  updateProject,
};
