const { getTokenData } = require("../../utils/jwt");
const { v4: uuidv4 } = require("uuid");

async function createCompany(parent, args, context, info) {
  const user = await context.prisma.user.findUnique({
    where: { email: getTokenData(context).username },
  });

  const localisation = `${args.company.street}, ${args.company.postal} ${args.company.city}, ${args.company.country}`;
  const date = new Date(args.company.creationDate);
  delete args.company.street;
  delete args.company.postal;
  delete args.company.city;
  delete args.company.country;
  delete args.company.creationDate;

  return {
    ...(await context.prisma.company.create({
      data: {
        ...args.company,
        User: { connect: { id: user.id } },
        id: uuidv4(),
        localisation,
        creation_date: date,
      },
    })),
    owner: user,
  };
}

module.exports = {
  createCompany,
};
