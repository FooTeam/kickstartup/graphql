async function discussion(parent, { id }, context) {
  return await context.prisma.discussion.findUnique({
    where: { id },
  });
}

async function getUserDiscussions(parent, { userId }, context) {
  return await context.prisma.discussion.findMany({
    where: { User: { id: userId } },
    include: { project: true }
  });
}

async function getProjectDiscussions(parent, { projectId }, context) {
  return await context.prisma.discussion.findMany({
    where: { project: { id: projectId } },
  });
}

module.exports = {
  discussion,
  getUserDiscussions,
  getProjectDiscussions,
};
