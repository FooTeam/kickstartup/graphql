function author(parent) {
  return parent.User;
}

async function messages(parent, args, context) {
  return await context.prisma.message.findMany({
    where: { discussion: { id: parent.id } },
  });
}

function createdAt(parent) {
  return parent.created_at;
}

module.exports = {
  author,
  messages,
  createdAt,
};
