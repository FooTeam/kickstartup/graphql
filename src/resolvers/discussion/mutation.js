const { getTokenData } = require("../../utils/jwt");
const { v4: uuidv4 } = require("uuid");

async function createDiscussion(parent, args, context, info) {
  const loggedUser = getTokenData(context);
  const { projectId, message } = args.discussion;
  return await context.prisma.discussion.create({
    data: {
      id: uuidv4(),
      User: {
        connect: { email: loggedUser.username },
      },
      project: {
        connect: { id: projectId },
      },
      message: {
        create: {
          id: uuidv4(),
          content: message.content,
          User: { connect: { email: loggedUser.username } },
          created_at: new Date(),
        },
      },
      created_at: new Date(),
    },
  });
}

module.exports = {
  createDiscussion,
};
