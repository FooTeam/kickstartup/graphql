const { getTokenData } = require("../../utils/jwt");
const { v4: uuidv4 } = require("uuid");
const { arrayCamelToSnakeCase } = require("../../helpers/array");

async function createApplication(parent, args, context, info) {
  const applicant = await context.prisma.user.findUnique({
    where: { email: getTokenData(context).username },
  });

  return await context.prisma.application.create({
    data: {
      User: { connect: { id: applicant.id } },
      id: uuidv4(),
    },
  });
}

async function updateApplication(parent, args, context) {
  const admin = await context.prisma.user.findUnique({
    where: { email: getTokenData(context).username },
  });

  if (admin.roles.includes("ROLE_ADMIN")) {
    const applicant = args.application.applicant;
    delete args.application.applicant;
    args.application = arrayCamelToSnakeCase(args.application);
    return await context.prisma.application.update({
      where: { applicant_id: applicant },
      data: args.application,
    });
  }
}

module.exports = {
  createApplication,
  updateApplication,
};
