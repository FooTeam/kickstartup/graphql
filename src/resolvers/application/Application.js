const { arraySnakeToCamelCase } = require("../../helpers/array");

async function applicant(parent, args, context) {
  let applicant = await context.prisma.application
    .findUnique({ where: { id: parent.id } })
    .User();
  if (applicant) {
    applicant = arraySnakeToCamelCase(applicant);
  }
  return applicant;
}

async function fullName(parent) {
  return parent.full_name;
}
async function companyName(parent) {
  return parent.company_name;
}
async function companySiret(parent) {
  return parent.company_siret;
}
async function companyLocalisation(parent) {
  return parent.company_localisation;
}

module.exports = {
  applicant,
  fullName,
  companyName,
  companySiret,
  companyLocalisation,
};
