const { getTokenData } = require("../../utils/jwt");

async function message(parent, { id }, context) {
  return await context.prisma.message.findUnique({
    where: { id },
  });
}

async function newMessages(parent, args, context) {
  return await context.prisma.message.findMany({
    where: {
      User: { email: { not: getTokenData(context).username } },
      seen: false,
    },
  });
}

module.exports = {
  message,
  newMessages,
};
