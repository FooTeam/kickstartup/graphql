const { getTokenData } = require("../../utils/jwt");
const { v4: uuidv4 } = require("uuid");

async function createMessage(parent, args, context) {
  const { discussionId, content } = args.message;
  return await context.prisma.message.create({
    data: {
      id: uuidv4(),
      content,
      User: {
        connect: { email: getTokenData(context).username },
      },
      discussion: {
        connect: { id: discussionId },
      },
      created_at: new Date(),
    },
  });
}

async function readMessage(parent, { id }, context) {
  return await context.prisma.message.udpate({
    where: { id },
    data: {
      seen: true,
    },
  });
}

module.exports = {
  createMessage,
  readMessage,
};
