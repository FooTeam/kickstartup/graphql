function author(parent) {
    return parent.User
}

function createdAt(parent) {
    return parent.created_at
}

module.exports = {
    author,
    createdAt
}
