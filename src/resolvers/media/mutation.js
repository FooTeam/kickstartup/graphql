const { me } = require("../user/query");

const processUpload = async (upload) => {
  // TODO: Implement processUpload() method.

  return {
    id: 1,
    alt: "this is a description",
    url: "https://url",
  };
};

const singleUpload = (obj, { file }) => processUpload(file);
const multipleUpload = (obj, { files }) =>
  Promise.all(files.map(processUpload));

module.exports = {
  singleUpload,
  multipleUpload,
};
