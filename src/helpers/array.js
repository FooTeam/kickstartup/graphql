const string = require("./string");

const arraySnakeToCamelCase = (arr) => {
  Object.keys(arr).forEach((key) => {
    arr[string.snakeToCamelCase(key)] = arr[key];
    if (string.snakeToCamelCase(key) !== key) delete arr[key];
  });
  return arr;
};

const arrayCamelToSnakeCase = (arr) => {
  Object.keys(arr).forEach((key) => {
    arr[string.camelToSnakeCase(key)] = arr[key];
    if (string.camelToSnakeCase(key) !== key) delete arr[key];
  });
  return arr;
};

module.exports = {
  arraySnakeToCamelCase,
  arrayCamelToSnakeCase,
};
