const https = process.env.NODE_ENV === 'dev' ? require('http') : require('https')
const { PrismaClient } = require("@prisma/client");

const mail = process.argv[2]
const name = process.argv[3]
const password = process.argv[4]
const data = JSON.stringify({
    "query": `mutation {signup(email: "${mail}", password: "${password}", name:"${name}", role:"admin"){user{id}}}`,
    "operationName": "",
})

const options = {
    hostname: 'localhost',
    port: process.env.NODE_ENV === 'dev' ? 4000 : 443,
    path: '/',
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
        'Content-Length': data.length
    }
}

const req = https.request(options, res => {
    res.on('data', async d => {
        process.stdout.write(d);
        process.exit();
    })
})
    
req.on('error', error => {
    console.error(error)
})

req.write(data)
req.end()
