const { GraphQLServer } = require("graphql-yoga");
const { PrismaClient } = require("@prisma/client");
const resolvers = require("./src/resolvers").resolvers;
const { PubSub } = require("graphql-yoga");
require("dotenv").config();

const prisma = new PrismaClient();
const pubsub = new PubSub();

const server = new GraphQLServer({
  typeDefs: "./src/schema.graphql",
  resolvers: resolvers,
  context: (request) => {
    return { ...request, prisma, pubsub };
  },
});
server.start(() => console.log(`🚀 app running at http://localhost:4000`));
